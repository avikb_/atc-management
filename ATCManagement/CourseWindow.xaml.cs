﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System;

namespace ATCManagement
{
    /// <summary>
    /// Interaction logic for Course.xaml
    /// </summary>
    public partial class CourseWindow : UserControl
    {
        private List<Control> Sinputs = new List<Control>();


        public CourseWindow()
        {
            InitializeComponent();

            Sinputs.Add(addBtn);
            Sinputs.Add(editBtn);
            Sinputs.Add(delBtn);

            courseListView.SelectionChanged += CourseListView_SelectionChanged;
            editBtn.IsEnabled = false;
            delBtn.IsEnabled = false;
        }

        private void CourseListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            bool ItemSelected = (((ListView)e.Source).SelectedIndex < 0 ? false : true);

            if (ItemSelected)
            {
                editBtn.IsEnabled = true;
                delBtn.IsEnabled = true;
            }
            else
            {
                editBtn.IsEnabled = false;
                delBtn.IsEnabled = false;
            }
        }

        public List<Control> GetControlArray()
        {
            return Sinputs;
        }

        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            ReloadList();
        }

        private void AddBtn_Click(object sender, RoutedEventArgs e)
        {
            AddCourse newAddCourseWindow = new AddCourse();
            newAddCourseWindow.NewItemAdded += NewAddCourseWindow_NewItemAddedEvent;
            newAddCourseWindow.ShowDialog();
        }

        /// <summary>
        /// Reload list when Event dispatcher for new itme added is invoked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <returns></returns>
        private void NewAddCourseWindow_NewItemAddedEvent(object sender, System.EventArgs e)
        {
            ReloadList();
        }

        public void ReloadList()
        {
            courseListView.Items.Clear();
            using (DatabaseEntities db = new DatabaseEntities())
            {
                var courseList = (from courses in db.Courses select courses).ToList();

                foreach (var course in courseList)
                {
                    courseListView.Items.Add(new CourseCls()
                    {
                        Id = course.Id.ToString(),
                        Name = course.Name,
                        Fee = course.Fee.ToString(),
                        CourseDuration = course.CourseDuration.ToString(),
                    });
                }

                db.Dispose();
            }
        }

        private void delBtn_Click(object sender, RoutedEventArgs e)
        {
            if (courseListView.SelectedIndex < 0)
            {
                return;
            }

            using (DatabaseEntities db = new DatabaseEntities())
            {
                long courseId;
                long.TryParse(((CourseCls)courseListView.SelectedValue).Id, out courseId);
                var courseToRemove = (from course 
                                      in db.Courses
                                      where course.Id == courseId select course).ToList<Course>();
                db.Courses.Remove(courseToRemove.First());
                db.SaveChanges();
                ReloadList();
            }
        }

        private void editBtn_Click(object sender, RoutedEventArgs e)
        {
            if (courseListView.SelectedIndex < 0)
            {
                return;
            }

            Int32 courseId = Convert.ToInt32(((CourseCls)courseListView.SelectedValue).Id);
            AddCourse newCourseWindow = new AddCourse(courseId);
            newCourseWindow.OnRecordUpdate += NewCourseWindow_OnRecordUpdate;
            newCourseWindow.ShowDialog();
        }

        private void NewCourseWindow_OnRecordUpdate(object sender, EventArgs e)
        {
            ReloadList();
        }
    }

    class CourseCls
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Fee { get; set; }
        public string CourseDuration { get; set; }
    }
}
