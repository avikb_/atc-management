﻿using System.Linq;
using System.Windows;

namespace ATCManagement
{
	/// <summary>
	/// Interaction logic for NewPayment.xaml
	/// </summary>
	public partial class NewPayment : Window
	{
		string studentid;
		public NewPayment(string SID)
		{
			InitializeComponent();
			studentid = SID;
		}

		private void Button_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				using (DatabaseEntities db = new DatabaseEntities())
				{
					var paymentList = (from payment in db.Payments select payment).ToList();

					foreach (var paymentData in paymentList)
					{
						if(paymentData.Date.Month == sinput3.SelectedDate.Value.Month && paymentData.Date.Year == sinput3.SelectedDate.Value.Year)
						{
							MessageBox.Show("Already paid");
							return;
						}
					}

					long newId = (db.Payments.Count() > 0) ? (from payment
											in db.Payments
															  orderby payment.Id
															  descending
															  select payment).First().Id + 1
										  : 0;

					long sid;
					long.TryParse(studentid, out sid);

					var studentData = (from student in db.Students where student.Id.ToString() == studentid select student).First();
					var courseData = (from course in db.Courses where course.Id == studentData.CourseId select course).First();

					long pamount;
					long.TryParse(courseData.Fee.ToString(), out pamount);

					Payment newPayment = new Payment()
					{
						Id = newId,
						StudentID = sid,
						Date = sinput3.SelectedDate.Value,
						PaidAmount = pamount,
					};

					db.Payments.Add(newPayment);
					db.SaveChanges();
				}

				this.Close();
			}
			catch (System.Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}
	}
}
