﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Data.SQLite;
using System.Linq;
using System.Diagnostics;

namespace ATCManagement
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        List<Control> InputStack = new List<Control>();

        private void Keypress(object sender, KeyEventArgs e)
        {
            bool navigate = false;
            int newTextboxId = 0;

            if (InputStack.Count == 0) return;

            if (e.Key == Key.Enter)
            {
                newTextboxId = InputStack.IndexOf(GetFocusedInput()) + 1;
                navigate = true;
            }

            if (navigate)
            {
                if (newTextboxId < InputStack.Count)
                {
                    Control newTextBox = InputStack[newTextboxId];
                    newTextBox.Focus();
                }
                navigate = false;
            }
        }
        private Control GetFocusedInput()
        {
            foreach (Control input in InputStack)
            {
                if (input.IsFocused)
                {
                    return input;
                }
            }

            return null;
        }

        private void AddForm(string header, UIElement form)
        {
            header hdr = new header(header);

            headerBorder.Children.Clear();
            bodyBorder.Children.Clear();

            headerBorder.Children.Add(hdr);
            bodyBorder.Children.Add(form);
        }

        private void CourseBtn_Click(object sender, RoutedEventArgs e)
        {
            InputStack.Clear();
            CourseWindow courseFrm = new CourseWindow();
            InputStack = courseFrm.GetControlArray();
            AddForm("Add & modify courses", courseFrm);
        }

        private void AddStudentBtn_Click(object sender, RoutedEventArgs e)
        {
            InputStack.Clear();
            StudentWindow addStudentFrm = new StudentWindow();
            InputStack = addStudentFrm.GetControlArray();
            AddForm("Add & modify Student data", addStudentFrm);
        }

        private void ViewStudentBtn_Click(object sender, RoutedEventArgs e)
        {

        }

        private void PayBtn_Click(object sender, RoutedEventArgs e)
        {
            InputStack.Clear();
            Payment paymentFrm = new Payment();
            InputStack = paymentFrm.GetControlArray();
            AddForm("Student Payment", paymentFrm);
        }

        private void FormLoaded(object sender, RoutedEventArgs e)
        {

        }

        private void aboutBtn_Click(object sender, RoutedEventArgs e)
        {
            Credit creditWIndow = new Credit();
            creditWIndow.ShowDialog();

        }
    }

    interface IUCForms
    {
        List<Control> GetControlArray();
    }

}
