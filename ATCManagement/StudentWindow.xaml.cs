﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using System;

namespace ATCManagement
{
    /// <summary>
    /// Interaction logic for StudentWindow.xaml
    /// </summary>
    public partial class StudentWindow : UserControl
    {

        List<Control> Sinputs = new List<Control>();

        public StudentWindow()
        {
            InitializeComponent();

            Sinputs.Add(addBtn);
            Sinputs.Add(editBtn);
            Sinputs.Add(delBtn);

            ReloadList();
            studentListView.SelectionChanged += StudentListView_SelectionChanged;
            editBtn.IsEnabled = false;
            delBtn.IsEnabled = false;
            paymentBtn.IsEnabled = false;
        }

        private void StudentListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            bool ItemSelected = (((ListView)e.Source).SelectedIndex < 0 ? false : true);

            if(ItemSelected)
            {
                editBtn.IsEnabled = true;
                delBtn.IsEnabled = true;
                paymentBtn.IsEnabled = true;
            }
            else
            {
                editBtn.IsEnabled = false;
                delBtn.IsEnabled = false;
                paymentBtn.IsEnabled = false;
            }
        }

        private void ReloadList(bool search = false)
        {
            studentListView.Items.Clear();
            using (DatabaseEntities db = new DatabaseEntities())
            {
                List<Student> studentList;
                if (search && !String.IsNullOrWhiteSpace(searchBox.Text))
                {
                    studentList = (from student in db.Students
                                   where student.StudentName.Contains(searchBox.Text)
                                   select student).ToList<Student>();
                }
                else
                {
                    studentList = (from student in db.Students select student).ToList();
                }


                foreach (var student in studentList)
                {
                    string sessionStartDate = (student.SessionStartDate != null) ? student.SessionStartDate : "";
                    var courseData = (from course in db.Courses where course.Id == student.CourseId select course).First();
                    studentListView.Items.Add(new StudentListCls()
                    {
                    Id = student.Id.ToString(),
                    StudentName = student.StudentName,
                    FatherName = student.FatherName,
                    Address = student.Address,
                    Phone = student.Phone.ToString(),
                    Course = courseData.Name,
                    SessionDate = sessionStartDate
                    });
                }
            }

        }

        internal List<Control> GetControlArray()
        {
            return Sinputs;
        }

        private void AddBtn_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            AddStudent newStudentWindow = new AddStudent();
            newStudentWindow.OnNewStudentAdded += NewStudentWindow_OnNewStudentAdded;
            newStudentWindow.ShowDialog();
        }

        private void NewStudentWindow_OnNewStudentAdded(object sender, System.EventArgs e)
        {
            ReloadList();
        }

        private void DelBtn_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if(studentListView.SelectedIndex < 0)
            {
                return;
            }

            using (DatabaseEntities db = new DatabaseEntities())
            {
                long studentId;
                long.TryParse(((StudentListCls)studentListView.SelectedValue).Id, out studentId);
                var studentDataToDelete = (from student in db.Students where student.Id == studentId select student).First();
                db.Students.Remove(studentDataToDelete);
                db.SaveChanges();

                ReloadList();
            }
        }

        private void EditBtn_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if(studentListView.SelectedIndex < 0)
            {
                return;
            }

            Int32 studentId = Convert.ToInt32(((StudentListCls)studentListView.SelectedValue).Id);
            AddStudent newStudentWindow = new AddStudent(studentId);
            newStudentWindow.OnRecordUpdated += NewStudentWindow_OnRecordUpdated;
            newStudentWindow.ShowDialog();
        }

        private void NewStudentWindow_OnRecordUpdated(object sender, EventArgs e)
        {
            ReloadList();
        }

        private void SearchFocus(object sender, System.Windows.RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(searchBox.Text) || searchBox.Text == "Search")
            {
                searchBox.Text = "";
            }
        }

        private void SearchUnfocused(object sender, System.Windows.RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(searchBox.Text))
            {
                searchBox.Text = "Search";
            }
        }

        private void OnSearchKeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if(e.Key == System.Windows.Input.Key.Enter)
            {
                SearchStudent();
            }
        }

        private void SearchBtn_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            SearchStudent();
        }

        private void SearchStudent()
        {
            ReloadList(true);
        }

        private void searchClearBtn_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            searchBox.Text = "";
            ReloadList(true);
            searchBox.Text = "Search";
        }

        private void paymentBtn_Click(object sender, System.Windows.RoutedEventArgs e)
        {

        }
    }

    class StudentListCls
    {
        public string Id { get; set; }
        public string StudentName { get; set; }
        public string FatherName { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Course { get; set; }
        public string SessionDate { get; set; }
    }
}
