﻿using System.Collections.Generic;
using System.Windows.Controls;
using System.Linq;
using System;
using System.Windows;

namespace ATCManagement
{
	public partial class Payment : UserControl, IUCForms
    {
		string StudentIDGlobal = "";
        List<Control> Sinputs = new List<Control>();

        public Payment()
        {
            InitializeComponent();
        }

        public List<Control> GetControlArray()
        {
            return Sinputs;
        }

        private void SearchFocused(object sender, System.Windows.RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(searchBox.Text) || searchBox.Text == "Enter Student ID")
            {
                searchBox.Text = "";
            }
        }

        private void SearchUnfocused(object sender, System.Windows.RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(searchBox.Text))
            {
                searchBox.Text = "Enter Student ID";
            }
        }

        private void searchClearBtn_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            searchBox.Text = "";
            searchBox.Text = "Enter Student ID";
			sName.Content = "";
			sName2.Content = "";
			sName3.Content = "";
			paymentListView.Items.Clear();
			StudentIDGlobal = "";
		}

        private void searchBtn_Click(object sender, System.Windows.RoutedEventArgs e)
        {
			GetStudentData();
        }

		private void GetStudentData()
		{
			paymentListView.Items.Clear();

			using (DatabaseEntities db = new DatabaseEntities())
			{
				if (!String.IsNullOrWhiteSpace(searchBox.Text))
				{
					try
					{
						var studentData = (from student in db.Students where student.Id.ToString() == searchBox.Text select student).First();
						var courseData = (from course in db.Courses where course.Id == studentData.CourseId select course).First();

						StudentIDGlobal = studentData.Id.ToString();


						sName.Content = studentData.StudentName;
						sName2.Content = courseData.Name;
						sName3.Content = studentData.Phone;

						var paymentList = (from payment in db.Payments select payment).ToList();

						foreach (var payment in paymentList)
						{
							paymentListView.Items.Add(new PaymentListCls()
							{
								Id = payment.Id.ToString(),
								Date = payment.Date.ToString(),
								PaidAmount = payment.PaidAmount.ToString(),
							});
						}
					}
					catch (Exception)
					{
						MessageBox.Show("Student ID not found!");
						sName.Content	= "";
						sName2.Content	= "";
						sName3.Content = "";
						paymentListView.Items.Clear();
						StudentIDGlobal = "";
					}
				}

			}
		}

		private void newPayBtn_Click(object sender, RoutedEventArgs e)
		{
			if(StudentIDGlobal == "")
			{
				MessageBox.Show("Please search student first");
				return;
			}
			NewPayment newpaymentWindow = new NewPayment(StudentIDGlobal);
			newpaymentWindow.ShowDialog();
		}
	}

	class PaymentListCls
	{
		public string Id { get; set; }
		public string Date { get; set; }
		public string PaidAmount { get; set; }
	}
}
