﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace ATCManagement
{
    public partial class AddCourse : Window
    {
        public event EventHandler NewItemAdded;
        public event EventHandler OnRecordUpdate;

        private List<Control> Sinputs = new List<Control>();
        private bool IsUpdating = false;
        private Int32 CourseId;

        public AddCourse(Int32 courseId = -1)
        {
            InitializeComponent();

            Sinputs.Add(sinput1);
            Sinputs.Add(sinput2);
            Sinputs.Add(addButton);

            if (courseId >= 0)
            {
                IsUpdating = true;
                CourseId = courseId;
                RestoreFromId(courseId);
            }
        }

        private void AddButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if(IsUpdating)
            {
                try
                {
                    using (var db = new DatabaseEntities())
                    {
                        Course newCourse = new Course()
                        {
                            Id = CourseId,
                            Name = sinput1.Text,
                            Fee = Convert.ToDecimal(sinput2.Text),
                            CourseDuration = Convert.ToInt32(sinput3.Text),
                            
                        };

                        db.Entry(newCourse).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

                OnRecordUpdate.Invoke(this, null);
                this.Close();
            }
            else
            {
                try
                {
                    using (var db = new DatabaseEntities())
                    {
                        long newID = 0;
                        var lastCouse = (from course in db.Courses orderby course.Id descending select course);
                        if(lastCouse.Count() > 0)
                        {
                            newID = lastCouse.First().Id + 1;
                        }

                        db.Courses.Add(new Course() {
                           Id = newID,
                           Name = sinput1.Text,
                           Fee = Convert.ToDecimal(sinput2.Text),
                           CourseDuration = Convert.ToInt32(sinput3.Text),
                        });
                        db.SaveChanges();
                    }

                    //Reload the list
                    NewItemAdded.Invoke(this, null);

                    //Clear the values from the input fields
                    sinput1.Text = string.Empty;
                    sinput2.Text = string.Empty;
                }
                catch (System.Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        public List<Control> GetControlArray()
        {
            return Sinputs;
        }

        private void RestoreFromId(Int32 courseId)
        {
            using (DatabaseEntities db = new DatabaseEntities())
            {
                var courseData = (from course
                                  in db.Courses
                                  where course.Id == courseId
                                  select course).First();

                sinput1.Text = courseData.Name;
                sinput2.Text = courseData.Fee.ToString();
                sinput3.Text = courseData.CourseDuration.ToString();
            }

            //update UI
            addButton.Content = "Update";
            this.Title = "Update Course Data";
        }

    }
}
