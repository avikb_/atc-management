﻿using System.Windows.Controls;

namespace ATCManagement
{
    /// <summary>
    /// Interaction logic for header.xaml
    /// </summary>
    public partial class header : UserControl
    {
        public header(string header)
        {
            InitializeComponent();
            header1.Content = header;
        }


    }
}
