﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Linq;

namespace ATCManagement
{
    /// <summary>
    /// Interaction logic for AddStudent.xaml
    /// </summary>
    public partial class AddStudent : Window, IUCForms
    {
        private List<Control> Sinputs = new List<Control>();
        private bool IsUpdating = false;
        private Int32 UpdatedStudentId;

        public event EventHandler OnNewStudentAdded;
        public event EventHandler OnRecordUpdated;

        public AddStudent(Int32 StudentId = -1)
        {
            InitializeComponent();
            Sinputs.Add(sinput1);
            Sinputs.Add(sinput2);
            Sinputs.Add(sinput3);
            Sinputs.Add(sinput4);
            Sinputs.Add(sinput5);
            Sinputs.Add(addButton);

            GetAllCourses();

            //If a valid student id is provided then try to update it instead of creating new student 
            if(StudentId >= 0)
            {
                IsUpdating = true;
                UpdatedStudentId = StudentId;
                RestoreFromId(StudentId);
            }
        }

        private void GetAllCourses()
        {
            //Populate the course list drop down 
            try
            {
                using (DatabaseEntities db = new DatabaseEntities())
                {
                    var listOfCourse = (from 
                                        courses 
                                        in db.Courses
                                        select courses).ToList<Course>();

                    foreach (var course in listOfCourse)
                    {
                        sinput5.Items.Add(course.Name);
                    }
                    sinput5.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public List<Control> GetControlArray()
        {
            return Sinputs;
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            //Check for empty fields!
            if (string.IsNullOrEmpty(sinput1.Text) || string.IsNullOrEmpty(sinput2.Text) || string.IsNullOrEmpty(sinput3.Text)
                || string.IsNullOrEmpty(sinput4.Text) || sinput5.SelectedItem == null)
            {
                MessageBox.Show("Please fill and select the appropiate options!", "Error!", MessageBoxButton.OK);
                return;
            }

            if (IsUpdating)
            {
                try
                {
                    using (DatabaseEntities db = new DatabaseEntities())
                    {
                        Student newStudent = new Student()
                        {
                            Id = UpdatedStudentId,
                            StudentName = sinput1.Text,
                            FatherName = sinput2.Text,
                            Phone = Convert.ToDecimal(sinput3.Text),
                            Address = sinput4.Text,
                            CourseId = sinput5.SelectedIndex,
                            SessionStartDate = sinput6.SelectedDate.Value.ToString()
                        };
                        db.Entry(newStudent).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                OnRecordUpdated.Invoke(this, null);
                this.Close();
            }
            else
            {
                try
                {
                    using (DatabaseEntities db = new DatabaseEntities())
                    {
                        long newId = (db.Students.Count() > 0)?(from student 
                                      in db.Students
                                      orderby student.Id 
                                      descending select student).First().Id + 1 
                                      : 0;

                        var courseId = (db.Courses.Count() > 0)?(from course 
                                        in db.Courses
                                        where course.Name == sinput5.SelectedValue.ToString()
                                        select course).First().Id
                                        : 0;

                        Student newStudent = new Student()
                        {
                            Id = newId,
                            StudentName = sinput1.Text,
                            FatherName = sinput2.Text,
                            Phone = Convert.ToDecimal(sinput3.Text),
                            Address = sinput4.Text,
                            CourseId = Convert.ToInt32(courseId),
                            SessionStartDate = (sinput6.SelectedDate == null)? 
                            DateTime.Now.ToString() 
                            : sinput6.SelectedDate.Value.ToString()
                        };
                        db.Students.Add(newStudent);
                        db.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

                OnNewStudentAdded.Invoke(this, null);
                sinput1.Text = string.Empty;
                sinput2.Text = string.Empty;
                sinput3.Text = string.Empty;
                sinput4.Text = string.Empty;
                sinput5.SelectedIndex = 1;
                sinput6.SelectedDate = DateTime.Now;
            }
        }

        private void RestoreFromId(Int32 StudentId)
        {
            using (DatabaseEntities db =  new DatabaseEntities())
            {
                var studentData = (from student 
                                   in db.Students
                                   where student.Id == StudentId
                                   select student).First();

                sinput1.Text = studentData.StudentName;
                sinput2.Text = studentData.FatherName;
                sinput3.Text = studentData.Phone.ToString();
                sinput4.Text = studentData.Address;
                sinput5.SelectedIndex = studentData.CourseId;
                sinput6.SelectedDate = DateTime.Parse(studentData.SessionStartDate);
            }

            //update UI
            addButton.Content = "Update";
            this.Title = "Update Student Data";
        }
    }
}
